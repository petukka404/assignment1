# CT70A3000 Software Maintenance
# Author: Petri Rämö 0438578
# Date: 19.01.2019

# variable a is the lenght of the series, variable b is the number which after the program counts the numbers
# variable e is the number that comes as outcome of this count

def fibo(a, b):
    
    if isinstance(a, int) != True or isinstance(b, int) != True or a < b:
        return "wrong"
    else:    
        f = open("output.txt", "w")
        c, d = 0, 1
        e = 0
        z = 1
        while z == 1:

            if a == 0:
                z = 0

            if b <= c and a != 0:
                e = e + 1
            a = a - 1
            c, d = d, c+d
        f.write(str(e))
        f.close()

        return e
